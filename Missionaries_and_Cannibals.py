def __solve(state, path, explored):
    if state == (0, 0, 0):
        path.append(state)
        explored.append(state)
        return path, explored

    if state[0] < 0 or state[0] > 3 or state[1] < 0 or state[1] > 3 or state in explored:
        return path, explored

    if state[1] > state[0] != 0 or (3 - state[1]) > (3 - state[0]) != 0:
        return path, explored

    # print(state)
    path.append(state)
    explored.append(state)

    if state[2] == 1:
        __solve((state[0] - 2, state[1], 0), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        __solve((state[0], state[1] - 2, 0), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        __solve((state[0] - 1, state[1], 0), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        __solve((state[0], state[1] - 1, 0), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        __solve((state[0] - 1, state[1] - 1, 0), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        path.remove(state)
        return path, explored

    else:
        __solve((state[0] + 2, state[1], 1), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        __solve((state[0], state[1] + 2, 1), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        __solve((state[0] + 1, state[1], 1), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        __solve((state[0], state[1] + 1, 1), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        __solve((state[0] + 1, state[1] + 1, 1), path, explored)
        if (0, 0, 0) in path:
            return path, explored
        path.remove(state)
        return path, explored


def run():
    return __solve((3, 3, 1), [], [])
