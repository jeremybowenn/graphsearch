import PriorityQueue as PQ
import math
def breadth_first_search(graph, start, goal):
    # TODO: Complete this method
    if start == goal:
        return []

    frontier = PQ.PriorityQueue()
    frontier.append((0, {start: []}))
    count = 1

    while frontier.has_next():
        current_cost, current_info = frontier.pop()
        for key in current_info.keys():
            current_city = key
            current_path = current_info[key]

        path = current_path

        if current_city == goal:
            path.append(current_city)
            return path

        for nb in graph[current_city]:
            # print(nb)
            new_path = current_path + [current_city]
            if nb not in frontier and nb not in graph.get_explored_nodes():
                if nb == goal:
                    path.append(current_city)
                    path.append(nb)
                    return path

                frontier.append((count, {nb: new_path}))
                count += 1
                # print(nb in frontier)

    return path


def uniform_cost_search(graph, start, goal):
    # TODO: Complete this method
    if start == goal:
        return []

    frontier = PQ.PriorityQueue()
    frontier.append((0, {start: []}))

    while frontier.has_next():
        current_cost, current_info = frontier.pop()
        # print(current_info)

        for key in current_info.keys():
            current_city = key
            current_path = current_info[key]

        path = current_path

        if current_city == goal:
            path.append(current_city)
            return path

        for nb in graph[current_city]:
            # print(graph[current_city][nb]['weight'])
            new_path = current_path + [current_city]
            if nb not in frontier and nb not in graph.get_explored_nodes():
                frontier.append((current_cost + graph[current_city][nb]['weight'], {nb: new_path}))
                # print(nb in frontier)

    return path


def euclidean_dist_heuristic(graph, v, goal):
    # TODO: Complete this method
    return math.sqrt(math.pow(graph.node[v]['pos'][0]-graph.node[goal]['pos'][0], 2) + math.pow(graph.node[v]['pos'][1] - graph.node[goal]['pos'][1], 2))


def a_star(graph, start, goal, heuristic=euclidean_dist_heuristic):
    # TODO: Complete this method
    if start == goal:
        return []

    frontier = PQ.PriorityQueue()
    frontier.append((0, {start: []}))

    while frontier.has_next():
        current_cost, current_info = frontier.pop()
        # print(current_info)

        for key in current_info.keys():
            current_city = key
            current_path = current_info[key]

        path = current_path

        if current_city == goal:
            path.append(current_city)
            return path

        for nb in graph[current_city]:
            # print(graph[current_city][nb]['weight'])
            new_path = current_path + [current_city]
            if nb not in frontier and nb not in graph.get_explored_nodes():
                frontier.append((current_cost + graph[current_city][nb]['weight'] + heuristic(graph, nb, goal), {nb: new_path}))
                # print(nb in frontier)

    return path


# Bonus Credit
def bidirectional_ucs(graph, start, goal):
    # TODO: Complete this method for Bonus
    return None


# Bonus Credit
def bidirectional_a_star(graph, start, goal, heuristic=euclidean_dist_heuristic):
    # TODO: Complete this method for Bonus
    return None


